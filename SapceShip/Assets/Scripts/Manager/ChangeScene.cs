﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ChangeScene : MonoBehaviour

{
    [SerializeField] private string sceneName;

    public void Continue()
    {
        if (SceneManager.GetActiveScene().buildIndex != null)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            Debug.Log("Continue!");
        }


    }

}

